package student.ntnu.idatt2001.magnusfarstad;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Bonus Member Test Class")
public class BonusMemberTest {

    public boolean passwordCheck(String password) {
        BonusMember bonusMember = new BonusMember(1, LocalDate.now(), 10000, "Jagsen, Jagge", "jagge@mail.com");
        bonusMember.setPassword("Hello World");
        return bonusMember.checkPassword(password);
    }

    @Nested
    @DisplayName("Password is correct")
    class passwordIsCorrect {

        @Test
        @DisplayName("Password is successfully validated")
        public void passwordIsSuccessfullyValidated() {
            assertTrue(passwordCheck("Hello World"));
        }
    }

    @Nested
    @DisplayName("Password is incorrect")
    class passwordIsIncorrect {

        @Test
        @DisplayName("Password is successfully not validated")
        public void passwordIsSuccessfullyNotValidated() {
            assertFalse(passwordCheck("Hello world"));
        }
    }

    @Nested
    @DisplayName("Member number is not supported")
    class memberNumberIsNotSupported {

        @Test
        @DisplayName("Member number is below 1")
        public void memberNumberIsBelowOne() {
            try {
                BonusMember bonusMember = new BonusMember(0, LocalDate.now(), 12000, "Finsen, Finn", "finsen@me.com");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "Member number must be larger than 0");
            }
        }
    }
}

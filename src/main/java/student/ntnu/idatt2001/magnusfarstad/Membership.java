package student.ntnu.idatt2001.magnusfarstad;

public abstract class Membership {

    public abstract int registerPoints(int bonusPointBalance, int newPoints);

    public abstract String getMembershipName();
}

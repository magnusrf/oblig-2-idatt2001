package student.ntnu.idatt2001.magnusfarstad;

import java.time.LocalDate;
import java.util.HashMap;

/**
 * The member archive holds all the bonus members. The archive provides
 * functionality for adding members to the register, looking up bonuspoints
 * of given members, registering new bonuspoints and listing all the members.
 *
 * @author arne
 */
public class MemberArchive {

    // Use a HashMap, since the members have a unique member number.
    private static HashMap<Integer, BonusMember> members;

    /**
     * Creates a new instance of MemberArchive.
     */
    public MemberArchive() {
        this.members = new HashMap<>();
        this.fillRegisterWithMembers();
    }

    public static HashMap<Integer, BonusMember> getMembers() {
        return members;
    }

    public int findPoints(int memberNumber, String password) {
        BonusMember member = members.get(memberNumber);
        int bonuspoints = -1;
        if(member.getPassword().equals(password)) {
            bonuspoints = member.getBonusPointsBalance();
        }
        return bonuspoints;
    }

    /**
     * Adds a new member to the register. The new member must have a memebr number
     * different from exsisting members. If not, the new member will not be added.
     *
     * @return {@code true} if the new member was added successfully,
     *         {@code false} if the new member could not be added, due to a
     *          membernumber that allready exsists.
     */
    public boolean addMember(BonusMember bonusMember) {
        boolean success = false;
        if(!(members.containsKey(bonusMember.getMemberNumber()))) {
            success = true;
            members.put(bonusMember.getMemberNumber(), bonusMember);
        }
        return success;
    }

    /**
     * Registers new bonuspoints to the member with the member number given
     * by the parameter {@code memberNumber}. If no member in the register
     * matches the provided member number, {@code false} is returned.
     *
     * @param memberNumber the member number to add the bonus points to
     * @param bonusPoints the bonus points to be added
     * @return {@code true} if bonuspoints were added successfully,
     *         {@code flase} if not.
     */
    public boolean registerPoints(int memberNumber, int bonusPoints) {
        boolean success = false;

        if(!(members.get(memberNumber) == null)) {
            success = true;
            members.get(memberNumber).registerBonusPoints(bonusPoints);
        }
        return success;
    }

    /**
     * Lists all members to the console.
     */
    public void listAllMembers() {
        for(int i = 1; i < members.size() +1 ; i++) {
            printMemberDetails(members.get(i));
        }
    }

    private void printMemberDetails(BonusMember bonusMember) {
        System.out.println(bonusMember.toString());
    }

    /**
     * Fills the register with some arbitrary members, for testing purposes.
     */
    private void fillRegisterWithMembers() {
        this.addMember(new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz"));
        this.addMember(new BonusMember(2, LocalDate.now(), 15000, "Jensen, Jens", "jens@jensen.biz"));
        this.addMember(new BonusMember(3, LocalDate.now(), 5000, "Lie, Linda", "linda@lie.no"));
        this.addMember(new BonusMember(4, LocalDate.now(), 30000, "Paulsen, Paul", "paul@paulsen.org"));
        this.addMember(new BonusMember(5, LocalDate.now(), 75000, "FLo, Finn", "finn.flo@gmail.com"));

    }
}

package student.ntnu.idatt2001.magnusfarstad;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.time.LocalDate;

/**
 * A simple client program to test the functionality of the
 * MemberArchive and underlying classes.
 *
 * @author arne
 */
public class MemberArchiveClient {

    public static void main(String[] args) {
        MemberArchive memberArchive = new MemberArchive();

        memberArchive.listAllMembers();
        memberArchive.getMembers().get(5).setPassword("Hello World");
        System.out.println("\nPassword is correct:");
        System.out.println(memberArchive.getMembers().get(5).checkPassword("Hello World"));

        System.out.println("\nAdd some bonuspoints to all of the members..\n");

        System.out.println("Member 1 gets 10.000");
        memberArchive.registerPoints(1, 10000);

        System.out.println("Member 2 gets 10.000");
        memberArchive.registerPoints(2, 10000);

        System.out.println("Member 3 gets 10.000");
        memberArchive.registerPoints(3, 10000);

        System.out.println("Member 4 gets 10.000");
        memberArchive.registerPoints(4, 10000);

        System.out.println("Member 5 gets 10.000");
        memberArchive.registerPoints(5, 10000);

        System.out.println("Now lets see the register:\n");
        memberArchive.listAllMembers();

        System.out.println();
        System.out.println("Bonus points balance for member number 5 is:");
        System.out.println(memberArchive.findPoints(5, "Hello World"));

    }
}

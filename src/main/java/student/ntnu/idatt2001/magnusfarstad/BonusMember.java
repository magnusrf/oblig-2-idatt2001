package student.ntnu.idatt2001.magnusfarstad;

import java.time.LocalDate;
import java.util.Objects;

public class BonusMember {
    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password;

    private Membership membership;

    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAddress) {
        /*if(memberNumber < 1) {
            throw new IllegalArgumentException("Member number must be larger than 0");
        } else*/
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.eMailAddress = eMailAddress;
        checkAndSetMembership();
    }


    public int getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(int memberNumber) {
        this.memberNumber = memberNumber;
    }

    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    public void setEnrolledDate(LocalDate enrolledDate) {
        this.enrolledDate = enrolledDate;
    }

    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    public void setBonusPointsBalance(int bonusPointsBalance) {
        this.bonusPointsBalance = bonusPointsBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public static int getSilverLimit() {
        return SILVER_LIMIT;
    }

    public static int getGoldLimit() {
        return GOLD_LIMIT;
    }

    public boolean checkPassword(String password) {
        boolean verify = false;
        if(this.password.equals(password)) {
            verify = true;
        }
        return verify;
    }

    public String getMembershipLevel() {
        return membership.getMembershipName();
    }

    private void checkAndSetMembership() {
        if(this.bonusPointsBalance >= GOLD_LIMIT) {
            setMembership(new GoldMembership());
        } else if(this.bonusPointsBalance < SILVER_LIMIT) {
            setMembership(new BasicMembership());
        } else setMembership(new SilverMembership());
    }

    public void registerBonusPoints(int newPoints) {
        int newBonusPointsBalance = membership.registerPoints(this.bonusPointsBalance, newPoints);
        setBonusPointsBalance(newBonusPointsBalance);
        checkAndSetMembership();
    }

    @Override
    public String toString() {
        return "BonusMember {" +
                "memberNumber = " + memberNumber +
                ", enrolledDate = " + enrolledDate +
                ", bonusPointsBalance = " + bonusPointsBalance +
                ", name = " + name + '\'' +
                ", eMailAddress = " + eMailAddress + '\'' +
                ", password = " + password + '\'' +
                ", membership = " + getMembershipLevel() +
                '}';
    }
}
